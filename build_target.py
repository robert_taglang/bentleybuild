import sublime, sublime_plugin, threading, os, subprocess, tempfile, json
from BentleyBuild.build_options_loader import BuildOptions

#-------------------------------------------------------------------------------------------
# bsiclass                                       Robert.Taglang      12/13
#-------------------------------------------------------------------------------------------
class BuildTargetCommand(sublime_plugin.ApplicationCommand):
    #-------------------------------------------------------------------------------------------
    # bsimethod                                      Robert.Taglang      12/13
    #-------------------------------------------------------------------------------------------
    def __init__(self):       
        self.shell = None
        self.process = None
        sublime_plugin.ApplicationCommand.__init__(self)

    #-------------------------------------------------------------------------------------------
    # bsimethod                                      Robert.Taglang      12/13
    #-------------------------------------------------------------------------------------------
    def run(self):
        text = "No shell environment set. Please select a script to set up your environment."
        window = sublime.active_window()
        if window.active_view().settings().has("bentleybuild_shell"):
            self.shell = window.active_view().settings().get("bentleybuild_shell")
        if self.shell is None:
            if sublime.ok_cancel_dialog(text):
                window.run_command("prompt_open_file")
                self.shell = window.active_view().file_name()
                self.add_setting("bentleybuild_shell", self.shell)
                window.focus_view(sublime.active_window().open_file(self.shell))
                window.run_command("close_file")
                startupinfo = None
            else:
                return
        if not self.shell is None:
            sublime.active_window().show_quick_panel(BuildOptions.get_options(), self.done)

    #-------------------------------------------------------------------------------------------
    # bsimethod                                      Robert.Taglang      12/13
    #-------------------------------------------------------------------------------------------
    def done(self, index):
        try:
            if index >= 0:
                part_name = BuildOptions.get_options()[index].split(".")[1]
                startupinfo = None
                if os.name == 'nt':
                    startupinfo = subprocess.STARTUPINFO()
                    startupinfo.dwFlags |= subprocess.STARTF_USESHOWWINDOW
                self.process = subprocess.Popen([self.shell, '&&', 'bentleybuild', 'r', part_name], shell=False, startupinfo=startupinfo, stdout = subprocess.PIPE, stderr = subprocess.STDOUT)
                output = PipeOutputToSublime(self.process, part_name)
                output.start()
        except Exception as e:
            print("Error while building: " + e)

    #-------------------------------------------------------------------------------------------
    # bsimethod                                      Robert.Taglang      12/13
    #-------------------------------------------------------------------------------------------
    def add_setting(self, key, value):
        settings_file = sublime.active_window().project_file_name()
        if settings_file is None or settings_file == "" or not os.path.isfile(settings_file):
            settings_file = os.path.join(sublime.packages_path(), "User/Preferences.sublime-settings")
        if settings_file is None or settings_file == "" or not os.path.isfile(settings_file):
            print("Unable to create permanent setting: " + str(key) + " : " + str(value))
            return
        handle = open(settings_file, 'r')
        raw = handle.read()
        handle.close()
        json_data = dict()
        if raw != "":
            json_data = json.loads(raw)
        if not "settings" in json_data.keys():
            json_data["settings"] = dict()
        json_data["settings"][key] = value
        handle = open(settings_file, 'w')
        print(str(json_data))
        handle.write(str(json_data).replace("\'", "\"").replace("True", "true").replace("False", "false"))
        handle.close()

#-------------------------------------------------------------------------------------------
# bsiclass                                       Robert.Taglang      12/13
#-------------------------------------------------------------------------------------------
class PipeOutputToSublime(threading.Thread):
    #-------------------------------------------------------------------------------------------
    # bsimethod                                      Robert.Taglang      12/13
    #-------------------------------------------------------------------------------------------
    def __init__(self, process, part_name):
        self.process = process;
        self.part_name = part_name
        threading.Thread.__init__(self)

    #-------------------------------------------------------------------------------------------
    # bsimethod                                      Robert.Taglang      12/13
    #-------------------------------------------------------------------------------------------
    def run(self):
        view = sublime.active_window().new_file()
        view.set_status("bentleybuild", "1")
        view.set_status("done", "0")
        while self.process.poll() is None:
            self.process.stdout.flush()
            text = self.process.stdout.readline().decode("utf-8")
            text = text.replace("\r", "")
            view.run_command("move_to", {"extend": False, "to": "eof"})
            view.run_command("insert", {"characters": text})
        view.set_status("done", "1")
        view.set_name(str(self.part_name)+".bbout")
#--------------------------------------------------------------------------------------
#
#     $Source: sublime/Packages/BentleyBuild/build_options_loader.py $
#
#  $Copyright: (c) 2013 Bentley Systems, Incorporated. All rights reserved. $
#
#--------------------------------------------------------------------------------------

import sublime, sublime_plugin, os, subprocess, threading, time, re, xml.etree.ElementTree as ET

package_name = "BentleyBuild"

#-------------------------------------------------------------------------------------------
# This thread trawls a directory, looks for files matching a file extension, and then spins
# off threads to handle those files. 
#
# It is used to find .PartFile.xml files in the graphite source tree.
#
# bsiclass                                       Robert.Taglang      10/13
#-------------------------------------------------------------------------------------------
class BuildTargetLoader(threading.Thread):

    parsers = []
    #-------------------------------------------------------------------------------------------
    # Constructor:
    # - directory = root directory to start search
    # - fileExt = the file extension that indicates a match
    # - callback = some object with a defined "action(self, parent, target, done)" method;
    # this will be called whenever a part gets loaded into memory
    #   - parent = name of partfile
    #   - target = specific build target inside of partfile
    #   - done = True if this is the last entry, False if it is not
    #
    # bsimethod                                      Robert.Taglang      10/13
    #-------------------------------------------------------------------------------------------
    def __init__(self, directories, fileExt, *callback):
        self.directories = directories
        self.fileExt = fileExt
        self.targets = BuildTargets(self, callback)
        self.done = False
        self.first = True
        threading.Thread.__init__(self)

    #-------------------------------------------------------------------------------------------
    # Overrides threading.Thread.run(self), will be called on self.start()
    #
    # bsimethod                                      Robert.Taglang      10/13
    #-------------------------------------------------------------------------------------------
    def run(self):
        for directory in self.directories:
            self.build_targets(directory)

    #-------------------------------------------------------------------------------------------
    # Directory trawling method. Searches all files and folders inside of the given
    # directory and spins off a BuildTargetXmlParser to handle each match for self.fileExt.
    #
    # bsimethod                                      Robert.Taglang      10/13
    #-------------------------------------------------------------------------------------------
    def build_targets(self, directory):
        first = self.first
        self.first = False
        climb = True
        if os.path.isdir(directory):
            files = os.listdir(directory)
            for child in files:
                if self.targets.get_should_stop():
                    return
                else:
                    if child.endswith(self.fileExt):
                        name = child.replace(self.fileExt, "")
                        parser = BuildTargetXmlParser(self.targets, name, os.path.join(directory, child))
                        BuildTargetLoader.parsers += [parser]
                        parser.start()
                        climb = False
            if climb:
                for child in files:
                    recurdir = os.path.join(directory, child)
                    if os.path.isdir(recurdir) and recurdir != directory:
                        self.build_targets(recurdir)
        if first:
            self.done = True

    #-------------------------------------------------------------------------------------------
    # Call this to end the loading operation before it has completed. This thread and all of 
    # the threads that it has created will stop.
    #
    # bsimethod                                      Robert.Taglang      10/13
    #-------------------------------------------------------------------------------------------
    def stop(self):
        self.targets.set_should_stop()
        active = None

    #-------------------------------------------------------------------------------------------
    # Test if this thread and all of its spin-off threads have completed their execution.
    # Returns true if they have, false if they have not
    #
    # bsimethod                                      Robert.Taglang      10/13
    #-------------------------------------------------------------------------------------------
    def is_done(self):
        if not self.done:
            return False
        else:
            for parser in BuildTargetLoader.parsers:
                if not parser.is_done():
                    return False
        return True

#-------------------------------------------------------------------------------------------
# Spin-off thread from BuildTargetLoader. This thread processes an individual XML file's 
# contents and notifies BuildTargets when one has been found.
#
# bsiclass                                       Robert.Taglang      10/13
#-------------------------------------------------------------------------------------------
class BuildTargetXmlParser(threading.Thread):

    #-------------------------------------------------------------------------------------------
    # Constructor:
    # - targets = BuildTargets object passed in by BuildTargetLoader
    # - parent = Name of file that is being processed, without its file extension
    # - xml = XML text to be parsed
    #
    # bsimethod                                      Robert.Taglang      10/13
    #-------------------------------------------------------------------------------------------
    def __init__(self, targets, parent, xml):
        self.xml = xml
        self.targets = targets
        self.parent = parent
        self.done = False
        threading.Thread.__init__(self)

    #-------------------------------------------------------------------------------------------
    # Overrides threading.Thread.run(self), will be called on self.start()
    # Searches for "Part" and "Product" tags in the xml file to identify build
    # parts.
    #
    # bsimethod                                      Robert.Taglang      10/13
    #-------------------------------------------------------------------------------------------
    def run(self):
        tree = ET.parse(self.xml)
        root = tree.getroot()
        for child in root:
            if self.targets.get_should_stop():
                return
            if child.tag == "Part" or child.tag == "Product":
                self.targets.add_target(self.parent, child.get("Name"));
        self.done = True

    #-------------------------------------------------------------------------------------------
    # Returns True if this individual thread has completed execution
    # Returns False if it has not
    #
    # bsimethod                                      Robert.Taglang      10/13
    #-------------------------------------------------------------------------------------------
    def is_done(self):
        return self.done

#-------------------------------------------------------------------------------------------
# bsiclass                                       Robert.Taglang      10/13
#-------------------------------------------------------------------------------------------
class BuildTargets:

    #-------------------------------------------------------------------------------------------
    # bsimethod                                      Robert.Taglang      10/13
    #-------------------------------------------------------------------------------------------
    def __init__(self, master, *callback):
        self.callback = callback
        self.shouldStop = False
        self.targets = []
        self.master = master

    #-------------------------------------------------------------------------------------------
    # bsimethod                                      Robert.Taglang      10/13
    #-------------------------------------------------------------------------------------------
    def add_target(self, parent, target):
        if not self.shouldStop:
            self.targets += [str(parent)+str(target)];
            for call in self.callback[0]:
                call.action(parent, target, self.master.is_done())

    #-------------------------------------------------------------------------------------------
    # bsimethod                                      Robert.Taglang      10/13
    #-------------------------------------------------------------------------------------------
    def get_should_stop(self):
        return self.shouldStop

    #-------------------------------------------------------------------------------------------
    # bsimethod                                      Robert.Taglang      10/13
    #-------------------------------------------------------------------------------------------
    def set_should_stop(self):
        self.shouldStop = True

    #-------------------------------------------------------------------------------------------
    # bsimethod                                      Robert.Taglang      10/13
    #-------------------------------------------------------------------------------------------
    def get_targets(self):
        return self.targets
            
started = False
#-------------------------------------------------------------------------------------------
# bsiclass                                       Robert.Taglang      12/13
#-------------------------------------------------------------------------------------------
class BuildOptions(sublime_plugin.EventListener):
    loader = None
    #-------------------------------------------------------------------------------------------
    # bsimethod                                      Robert.Taglang      12/13
    #-------------------------------------------------------------------------------------------
    def on_activated(self, view):
        global started
        if not started:
            BuildOptions.loader = BuildTargetLoader(sublime.active_window().folders(), "PartFile.xml");
            started = True
            BuildOptions.loader.start()

    def on_selection_modified_async(self, view):
        text = None
        file_ext = ""
        if len(view.name().split(".")) > 1:
            file_ext = view.name().split(".")[1]
        if view.get_status("bentleybuild") == "1" or file_ext == "bbout":
            if view.get_status("done") == "1":
                selection = view.sel()[0]
                for region in view.get_regions("error"):
                    if selection.intersects(region):
                        text = view.substr(region)
                        break
                if text is None:
                    for region in view.get_regions("warning"):
                        if selection.intersects(region):
                            text = view.substr(region)
                            break
                if not text is None:
                    paren = re.compile(r'\((.*?)\)')
                    for match in paren.finditer(text):
                        line_num = int(match.group(0).replace("(", "").replace(")", ""))
                        file_name = text[0:match.start()]
                        opened = sublime.active_window().open_file(file_name)
                        show_region = opened.split_by_newlines(sublime.Region(0,view.size()))[line_num-1]
                        opened.sel().clear()
                        opened.sel().add(show_region)     
                        opened.show_at_center(show_region)             

    def on_modified_async(self, view):
        file_ext = ""
        if len(view.name().split(".")) > 1:
            file_ext = view.name().split(".")[1]
        if view.get_status("bentleybuild") == "1" or file_ext == "bbout":
            text = view.substr(sublime.Region(0, view.size()))
            errors = re.compile(r"error C[0-9]{4}:")
            warnings = re.compile(r"warning C[0-9]{4}:")
            for error in errors.finditer(text):
                view.add_regions("error", view.get_regions("error") + [view.line(error.start())], scope="message.error")
            for warning in warnings.finditer(text):
                view.add_regions("warning", view.get_regions("warning") + [view.line(warning.start())], scope="Invalid")


    #-------------------------------------------------------------------------------------------
    # bsimethod                                      Robert.Taglang      12/13
    #-------------------------------------------------------------------------------------------
    @staticmethod
    def get_options():
        return BuildOptions.loader.targets.get_targets();
